package generic;

public class minMax {
    public static < E extends Comparable<E>> void printArray( E[] inputArray ) {
        // Display array elements
        E max = inputArray[0];
        for(E element : inputArray) {
            if(element.compareTo(max) > 0){max = element;}
        }
        System.out.println(max + "is the max");
    }

    public static void main(String[] args) {
        Integer[] a = {1,2,3,4};
        printArray( a   );
    }
}
